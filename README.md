# GGSV2

[![Hex version](https://img.shields.io/hexpm/v/ggsv2.svg "Hex version")](https://hex.pm/packages/ggsv2)

Driver for the [Grove - Multichannel Gas Sensor v2](https://www.seeedstudio.com/Grove-Multichannel-Gas-Sensor-v2-p-4569.html)

## Installation

This package can be installed by adding `ggsv2` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:ggsv2, "~> 0.1"}
  ]
end
```

## Sample usage

### Using sensor commands

```elixir
{bus_name, device_addr} = GGSV2.Comm.discover
i2c = GGSV2.Comm.open(bus_name)
GGSV2.PREHEAT.activate(i2c, device_addr)
GGSV2.GAS.read_no2(i2c, device_addr)
GGSV2.GAS.read_c2h5ch(i2c, device_addr)
GGSV2.GAS.read_voc(i2c, device_addr)
GGSV2.GAS.read_co(i2c, device_addr)
GGSV2.GAS.read_all(i2c, device_addr)
```

### Using GenServer

```elixir
iex(1)> GGSV2.start_link([name: :ggsv2])
{:ok, #PID<0.1393.0>}
iex(2)> :timer.sleep(20_000)
:ok
iex(3)> GenServer.call(:ggsv2, :measure)
{:ok,
 %GGSV2.Measurement{no2_ppm: 330, c2h5ch_ppm: 380, voc_ppm: 147, co_ppm: 147}}
```


