defmodule GGSV2.I2C do
  @moduledoc """
  I2C wrappers introducing delays.
  """

  def write_read(i2c_bus, address, write_data, bytes_to_read, opts \\ []) do
    :ok = Circuits.I2C.write(i2c_bus, address, write_data, opts)
    Circuits.I2C.read(i2c_bus, address, bytes_to_read, opts)
  end

  def write(i2c_bus, address, write_data, opts \\ []) do
    :ok = Circuits.I2C.write(i2c_bus, address, write_data, opts)
    :timer.sleep(10)
    :ok
  end

  def read(i2c_bus, address, bytes_to_read, opts \\ []) do
    Circuits.I2C.read(i2c_bus, address, bytes_to_read, opts)
  end
end
