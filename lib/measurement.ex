defmodule GGSV2.Measurement do
  @type t() :: %__MODULE__{
          no2_ppm: Float.t(),
          c2h5ch_ppm: Float.t(),
          voc_ppm: Float.t(),
          co_ppm: Float.t()
        }

  defstruct no2_ppm: :unknown,
            c2h5ch_ppm: :unknown,
            voc_ppm: :unknown,
            co_ppm: :unknown
end
