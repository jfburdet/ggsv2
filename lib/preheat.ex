defmodule GGSV2.PREHEAT do
  alias Circuits.I2C

  @warming_up [0xFE]
  @warming_down [0xFE]

  def activate(i2c, device_addr) do
    I2C.write(i2c, device_addr, [
      @warming_up
    ])
  end

  def deactivate(i2c, device_addr) do
    I2C.write(i2c, device_addr, [
      @warming_down
    ])
  end
end
